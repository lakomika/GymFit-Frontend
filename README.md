# GymFit-Frontend

Frontend of an application supporting gym management.

## Table of contents

* [General info](#general-info)
* [Demo](#demo)
* [Roles](#roles)
* [Technologies](#technologies)
* [Example](#example)

## General info

The application implemented is used to manage gym passes. Nowadays, buying and selling a pass is a basic operation at
sports and recreation facilities. In application are 3 roles:
<ul>
<li><b>Customer</b></li>
<li><b>Receptionist</b></li>
<li><b>Administrator</b></li>
</ul>

A discussion of the possibility of each role is set out on
Backend [page](#https://gitlab.com/lakomika/GymFit-Backend#roles).

## Demo

<p>Here is a working live demo : https://lakomika-gym-fit.herokuapp.com/ </p>
<p>(Please wait a moment as the server may be starting up. :smile:) </p>
<p>Example login data for:</p>
<p><b>Administrator:</b></p>
<p>Login: <i>admin.example</i> Password: <i>SuperSecretPassword532#</i></p>
<p><b>Customer:</b></p>
<p>Login: <i>customer.example</i> Password: <i>CustomerSecretPassword532#</i></p>
<p><b>Receptionist:</b></p>
<p>Login: <i>receptionist.example</i> Password: <i>ReceptionistSecretPassword532#</i></p>

## Roles

A discussion of the possibility of each role is set out on
Backend [page](#https://gitlab.com/lakomika/GymFit-Backend#roles).

## Technologies

* Angular 10.0.14
* Angular Material 10.0.14
* Jquery 3.5.1
* Bootstrap 4.5.0
* I18n 0.13.2
* Ng-recaptcha (Version 2 - Driver 7.0.1)
* Ng2-validation 4.2.0

## Example

### PL Version

#### Main page:

![Main page](http://lakomika.pl/GitHub/GymFit/PL_Main_Page.PNG)

#### Register page:

![Register_page_1](http://lakomika.pl/GitHub/GymFit/PL_Register_Page_1.PNG)
![Register_page_2](http://lakomika.pl/GitHub/GymFit/PL_Register_Page_2.PNG)

#### Register page (Success):

![Register page (Success](http://lakomika.pl/GitHub/GymFit/PL_Register_Page_Window_Success.PNG)

#### Login page:

![Login page](http://lakomika.pl/GitHub/GymFit/PL_Login_Page.PNG)

#### Login page (with invalid login data):

![Login page_with_error](http://lakomika.pl/GitHub/GymFit/PL_Login_Page_Error.PNG)

#### Client main page:

![Client main page](http://lakomika.pl/GitHub/GymFit/PL_Main_Page_Client.PNG)

#### Administrator main page:

![Administrator main page](http://lakomika.pl/GitHub/GymFit/PL_Main_Page_Administrator.PNG)

#### Verification of the validity of the pass by the administrator:

![Verification of the validity of the pass by the administrator](http://lakomika.pl/GitHub/GymFit/PL_verification_of_the_validity_of_the_pass_by_the_administrator.PNG)

##### 1.Pass invalid:

![Verification of the validity of the pass by the administrator_pass_invalid](http://lakomika.pl/GitHub/GymFit/PL_verification_of_the_validity_of_the_pass_by_the_administrator_pass_invalid.PNG)

##### 2.Pass valid:

![Verification of the validity of the pass by the administrator_pass_valid](http://lakomika.pl/GitHub/GymFit/PL_verification_of_the_validity_of_the_pass_by_the_administrator_pass_valid.PNG)

##### 3.No pass in the database:

![No pass in the database](http://lakomika.pl/GitHub/GymFit/PL_No_Pass_In_The_Database.PNG)
